import numpy as np

def read_from_file(filename, sep=None):
    nodes = []
    f = open(filename, 'r')
    for l in f:
        p = l.split(sep)
        nodes.append([int(p[0]), int(p[1])])
    f.close()

    return np.array(nodes)

def write_to_file(filename, nodes, clusters, sep=' '):
    if len(nodes) != len(clusters):
        raise ValueError('Length of nodes and clusters does not match.')

    f = open(filename, 'w')
    for i in xrange(len(nodes)):
        f.write("%d%c%d%c%d\n" % (nodes[i][0], sep, nodes[i][1], sep, clusters[i]))
    f.close()

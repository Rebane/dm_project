import math
import random
import numpy as np
import sys


"""
1. Input parameters numlocal and maxneighbor. Initialize i to 1, and mincost to a large number.
2. Set current to an arbitrary node in G_{n,k}
3. Set j to 1.
4. Consider a random neighbor S of current, and based on Equation (5) calculate the cost differential 
    of the two nodes.
5. If S has a lower cost, set current to S, and go to Step (3).
6. Otherwise, increment j by 1. If j<=maxneighbor,go to Step (4).
7. Otherwise, when j > maxneighbor, compare the cost of current with mincost. If the former is less than mincost, 
    set mincost to the cost of current, and set bestnode to current.
8. Increment i by 1. If i > numlocal, output bestnode and halt. Otherwise, go to Step (2).
"""

def clarans_teq(points, numlocal, maxneighbor, mincost,k):
#    random.seed(1)
#    np.random.seed(1)
    i=1
    N = len(points)
    best = 9999999999
    
    d_mat = np.asmatrix(np.empty((k,N)))
    medoid_distances = np.asmatrix(np.empty((k,k)))
    bestnode = []
    
    while i<=numlocal:
        #Select a current node randomly and calculate the distances between medoids, where each node is the collection of k medoids. 
        node = np.random.permutation(range(N))[:k]
        #node = list(node)
        med_distances(medoid_distances,points, node)
        
        # The average distance of this current node is computed using TIE and PDS. 
        # Record the index of the medoid for each object.
        # TODO
        fill_distances(d_mat, medoid_distances, points, node)     
        cls = assign_to_closest(points, node, d_mat)   
        cost = total_dist(d_mat, cls)
        copy_node = node.copy()
        print 'new start \n'

        #increase neighbor count
        j = 1 
        
        while j<=maxneighbor:
            #Select a neighbor node randomly and update the medoid distances between medoids.
            local_best = copy_node.copy()
            idx = pick_random_neighbor(copy_node, N)
            update_med_distances(medoid_distances,points,copy_node,idx)
            
            #Calculate the distance between the object and the previous medoid index of this object, this distance is referred to as Dmin
            #TODO
            update_distances(d_mat, points, copy_node, idx)            
            cls = assign_to_closest(points, copy_node, d_mat)   
            new_cost = total_dist(d_mat, cls)
            print str(best) + ' ' +str(mincost) + ' '  + str(cost) + ' ' + str(new_cost) + '\n'
            if new_cost < best:
                best = new_cost
            
            #check if new cost is smaller 
            if new_cost < cost:
                cost = new_cost
                print 'Best cost: ' + str(cost) + ' '
                print local_best 
                print '\n'
                j = 1
                continue
            else:
                copy_node = local_best.copy()
                j=j+1
                if j<=maxneighbor:
                    continue
                elif j>maxneighbor:
                    if mincost>cost:
                        mincost = cost
                        bestnode = copy_node
            i = i+1
            if i>numlocal:
                fill_distances(d_mat,medoid_distances, points, bestnode)     
                cls = assign_to_closest(points, bestnode, d_mat)
                print 'BEST: ' + str(best)
                return cls, bestnode
            else:
                break
    
    
def pick_random_neighbor(current_node, set_size):
    #pick a random item from the set and check that it is not selected
    node = random.randrange(0, set_size, 1)
    while node in current_node:
        node = random.randrange(0, set_size, 1)
    #replace a random node
    i = random.randrange(0, len(current_node))
    current_node[i]=node
    return i
    
def dist_euc((x1, y1), (x2, y2)):
    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def assign_to_closest(points, meds, d_mat):
    cluster =[]
    for i in xrange(len(points)):
        if i in meds:
            #cluster.append(meds.index(i))
            cluster.append(np.where(meds==i))
            continue   
        d = sys.maxint
        idx=i
        for j in xrange(len(meds)):
            d_tmp = d_mat[j,i]
            if d_tmp < d and d_tmp!=-1:
                d = d_tmp
                idx=j
        cluster.append(idx)
    return cluster


def fill_distances(d_mat, d_med, points, current_node):
    for i in range(len(points)):
        for k in range(len(current_node)):
            if d_mat[k,i]!=-1:
                d_mat[k,i]=dist_euc(points[current_node[k]], points[i])
                for l in range(k,len(current_node)):
                    if d_med[k,l]>=2*d_mat[k,i]:
                        d_mat[k,l]=-1
                    
    
def med_distances(medoids_mat, points, current_node):
    for i in range(len(current_node)):
        medoids_mat[i,i]=0
        for j in range(i+1, len(current_node)):
            medoids_mat[i,j]=dist_euc(points[current_node[i]], points[current_node[j]])
            medoids_mat[j,i]=medoids_mat[i,j]

        
def total_dist(d_mat, cls):
    tot_dist = 0
    for i in xrange(len(cls)):
        tot_dist += d_mat[cls[i],i]
    return tot_dist


def update_distances(d_mat, points, node, idx):
    for j in range(len(points)):
        d_mat[idx,j]=dist_euc(points[node[idx]], points[j])
        
def update_med_distances(medoid_distances,points,node,idx):
    for i in range(len(medoid_distances)):
        medoid_distances[idx,i]=dist_euc(points[node[idx]], points[node[i]])
        medoid_distances[i,idx]=medoid_distances[idx,i]

from clarans import clarans_basic
from clarans_teq import clarans_teq
import cProfile
import sys

#cluster    
def output_clusters(points, medoids, medoid_points):
    for i in range(len(points)):
        medoid_points[i]=medoids[medoid_points[i]]
        
        
def write_to_file(filename, nodes, clusters, sep=' '):
    if len(nodes) != len(clusters):
        raise ValueError('Length of nodes and clusters does not match.')

    f = open(filename, 'w')
    for i in xrange(len(nodes)):
        f.write("%d%c%d%c%d\n" % (nodes[i][0], sep, nodes[i][1], sep, clusters[i]))
    f.close()
    
    
#Set parameters
k = 8
numlocal = 5
maxneighbour = 70
mincost = sys.maxint


#Read data
#f = open("data.txt", "r")
#f = open("test_data_1000.txt", "r")
f = open("nice_clusters_500_8.txt", "r")
#f = open("test_data_clusters_1000.txt", "r")
points = f.readlines()
for j in range(len(points)):
    coordinates = points[j].split()
    coordinates[0]=int(coordinates[0])
    coordinates[1]=int(coordinates[1])
    #print coordinates
    points[j]=tuple(coordinates)
    
    

medoid_points, medoids = clarans_teq(points, numlocal, maxneighbour,  mincost,k)

#medoid_points, medoids = clarans_teq(points, numlocal, maxneighbour,  mincost, k)
output_clusters(points, medoids, medoid_points)
write_to_file('output.txt', points, medoid_points)
print 'ready'

#print medoid_points
#print "medoids:\n", medoids

# clusters is a dictionary of indexes and corresponding medoids
# clusters, medoids = clarans_basic(points, numlocal, maxneighbour,  mincost,k, i )

#clusters, medoids = clarans_teq(points, numlocal, maxneighbour,  mincost,k, i )
#cProfile.runctx('clarans_teq(points, numlocal, maxneighbour,  mincost,k)', 
#                globals(), 
#                {'points':points, 'numlocal':numlocal, 
#                            'maxneighbour':maxneighbour, 'mincost':mincost, 
#                            'k':k})
#
#cProfile.runctx('clarans_basic(points, numlocal, maxneighbour,  mincost,k)', 
#                globals(), 
#                {'points':points, 'numlocal':numlocal, 
#                            'maxneighbour':maxneighbour, 'mincost':mincost, 
#                            'k':k})


#print "\nclusters:S"
#print get_key(clusters,0)
#print get_key(clusters,1)
#print get_key(clusters,2), '\n'
#
#




 

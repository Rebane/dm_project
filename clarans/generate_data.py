import random

data_size = 100

#for i in range(data_size):
#    if i<data_size/4:
#        pt = random.randint(0, data_size/4)
#        pt2 = random.randint(0, data_size/4)
#    elif i>data_size/4 and i<data_size/2:
#        pt = random.randint(data_size/4, data_size/2)
#        pt2 = random.randint(data_size/4, data_size/2)
#    elif i>data_size/2 and i<data_size*0.75:
#        pt = random.randint(data_size/2, data_size*0.75)
#        pt2 = random.randint(data_size/2, data_size*0.75)
#    else:
#        pt = random.randint(data_size*0.75, data_size)
#        pt2 = random.randint(data_size*0.75, data_size)
#           
#    f.write(str(pt)+' '+str(pt2)+'\n')
    
def generate_clusters(axis_length, n, size, file_name):
    f = open(file_name, "w")
    step = axis_length / n
    if n % 2 == 0:
        num_points = size / (n**2/2)
    else:
        num_points = size / ((n**2)/2 + 1)
    for i in xrange(n):
        for j in xrange(n):
            if i % 2 == j % 2:
                for k in xrange(num_points):
                    f.write("%d %d\n" % (random.randint(i*step,(i+1)*step),random.randint(j*step,(j+1)*step)))
        
axis_length = 1000    
n = 4         
size = 500 

file_name = 'nice_clusters_500_13.txt'
generate_clusters(axis_length, n, size, file_name)
\documentclass{llncs}
\usepackage{amsfonts}     % Fonts for math
\usepackage{url}
\usepackage{color}
\usepackage{graphicx}	
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\newcommand{\TODO}[1]{TODO: \textcolor{red}{#1}}

\begin{document}
\title{Scalable K-medoids using triangle inequality}
\author{Reimo Rebane, Kristjan Krips}
\institute{University of Tartu, Estonia}
\maketitle
 
\begin{abstract}
Simple clustering algorithms like K-medoids are not very scalable. This is caused by the fact that the problem of clustering n objects 
into k clusters is NP-hard. In order to make K-medoids scalable we will use heuristics combined with triangle inequality to reduce the 
number of required distance calculations. 
\end{abstract}


\section{Introduction}
Clustering is widely used in data mining and simplifies the analysis of many problems. E.g. clustering helps to find patterns financial, 
medical and biological. Clustering methods are usually taught using simple and naive algorithms that are actually not used with real 
problems. E.g. the naive algorithm of K-medoids that clusters n objects into k clusters searches through all possible clusterings to 
find the best solution. This is not efficient when data has thousands or millions of objects that have to be clustered. 
Therefore the naive algorithms have to be improved to make them scalable.\\ 

\noindent In this project we describe the shortages of the standard K-medoids algorithm 
and improve it by using triangle inequality for calculating distances. Triangle inequality allows to avoid some distance calculations 
and to make it more scalable.  
However, as the complexity 
of finding an optimal clustering is NP-hard, we cannot improve the clustering algorithms only by standard means and we have to use some 
heuristics or randomness. Therefore, the solution of the problem is not guranteed to be the best, we can only say that with a certain 
probability the solution is good enough. However, this problem can be somewhat solved by running the probabilistic clustering algorihm 
several times.\\ As a result of the project we implemented an improved version of K-medoids. 
The analysis and comparison of the basic and improved algorithm is given in the following sections.  


\section{Basic algorithm and improvement ideas}
\subsection*{Partitioning Around Medoids}
The most common algorithm for K-medoids is Partitioning Around Medoids(PAM). It starts by randomly selecting inital k starting points 
(medoids) and then mapping each data point to the closest medoid. The medoids are changed until there are no better solutions. 
A distance metric like Euclidean distance is used for finding the closest medoid. 
However, this requires calculating distances between all chosen medoids and data points. Therefore, this algorithm 
is not scalable. For more information about PAM, see \cite{Ng:2002:CMC:627342.628263}.\\

\subsection{Ideas for improvement}
\noindent We can think of the clustering problem as searching through a graph. A node $G_{n,k}$ in this graph is the set 
of k chosed medoids and the graph contains all possible k subsets of the n objects. 
We say that the nodes are neighbours if they differs by only one medoid.\\ 
A solution for the scaling problem is to avoid looking through all neighbours of the nodes and to search through a random 
set of neighbours. The algorithm that uses this idea is named Clustering Large Applications based
on RANdomized Search(CLARANS). CLARANS outperforms PAM even with small data sets and the speed difference is significant when the 
number of data points grows. For more information about CLARANS, see \cite{Ng:1994:EEC:645920.672827,Ng:2002:CMC:627342.628263}\\
 
\noindent Another way to make the algorihm more scalable is to reduce the number of distance calculations. For that we can 
use triangular inequality. Triangular inequality is defined in the Euclidean space so that for points $x,y,z$ 
\begin{equation} \label{eq:triangular_inequality}
 d(x,y)+d(y,z)\geq d(x,z).
\end{equation}

\noindent We can use this property to avoid some distance calculations. 
Namely, it follows from (\ref{eq:triangular_inequality}) that for points 
$A,B,C$,\\

\noindent if
\begin{equation} \label{triangular_inequality}
 d(B,C)\geq 2\cdot d(A,B),
\end{equation}
\noindent then
\begin{equation} \label{triangular_inequality}
 d(A,C)\geq d(A,B).
\end{equation}
\noindent An illustration of this is shown on the Figure ~\ref{fig:triangular_inequality}.

\noindent To use this feature, we implemented a modification of CLARANS, which uses triangular inequality elimination (TIE), 
previous medoid index and partial distance search (PDS), which may allow to skip the distance calulations on some dimensions. 
The modified algorithm is called CLARANS-ITP, for more information about the algorithm, see \cite{Chu:2002:EKM:646111.679595}.


\begin{figure}[h!t]
\centering
\includegraphics[scale=0.3]{tri1.jpg}
\caption{As distance $d(A,B)\leq \frac{1}{2}\cdot d(B,C)$, then $d(A,B)\leq d(A,C)$.}
\label{fig:triangular_inequality}
\end{figure}


\section{Impoved K-medoids}
Now we will describe how we improved the standard K-medoids algorithm to make it scalable. 
The first step towards making the algorithm scalable is to avoid looking through a large percentage of all possible medoids. 
For that we implemented CLARANS algorithm which uses randomness for selecting the medoids. To improve it further, we added 
triangular inequality to the CLARANS algorithm, this was described in \cite{Chu:2008:ISS:1413814.1413821}. In order to use 
triangular inequality in CLARANS algorithm we will have to calculate the distance between all pairs of medoid and update this 
distance when the medoids change. Besides that, after calculating the distance between a data point and a medoid we will have to 
search through all remaining medoid pairs to check if we can apply triangular inequality. These additional distance calculations and 
checks will add some complexity to the algorithm. However, the benefit of avoiding a large number of distance calculations should be bigger.

\section{Testing and comparisons}
We implemented the algorithms PAM, CLARANS and CLARANS-ITP in python and used Euclidean distance as the cost measure for clustering. 
In CLARANS-ITP we implemented previous medoid index, partial distance search and triangular inequality elimination.
We did not use many optimization techniques in the implementation. 
Therefore, we cannot compare the speed of our implementation to the optimized implementations written in C/C++. 
We will therefore compare the running times of the 
algorithms that we implemented. We will show the difference of average running time between PAM, CLARANS and CLARANS with triangular 
inequality.\\

\subsection{Testing data}
\noindent The testing was done on generated data. We generated two-dimensional data that was clustered in the shape of the chessboard. 
For example, the Figure~\ref{fig:data_example} shows clustered test data which has eight clusters and 500 data points. 

\begin{figure}[h!t]
\centering
\includegraphics[scale=0.3]{clarans_data.pdf}
\caption{An example of the generated data which was used for testing.}
\label{fig:data_example}
\end{figure}


\subsection{Testing results}
The first testing was made to compare the quality and time complexity of clustering with PAM and CLARANS. The testing data was generated in the previously 
described manner and ranged from 100 to 400 data points with 100 point steps. The data was generated and tested in two ways, with two clusters and with five clusters.

\begin{figure}[h!t]
\centering
\includegraphics[scale=0.45]{time.png}
\caption{Total distance of the clustering, comparison of PAM and CLARANS.}
\label{fig:PAM_CLARANS_distance}
\end{figure}

\noindent The second testing was made to compare the quality and time complexity of clustering with CLARANS and CLARANS-ITP. The testing data was generated in the previously 
described manner and ranged from 500 to 1500 data points with 500 point steps. The data was generated and tested in three ways, with two clusters, with five clusters and with eight clusters.


\begin{figure}[h!t]
\centering
\includegraphics[scale=0.45]{distance.png}
\caption{Time complexity of the clustering, comparison of PAM and CLARANS.}
\label{fig:PAM_CLARANS_time}
\end{figure}

\begin{figure}[h!t]
\centering
\includegraphics[scale=0.55]{cc_distance.png}
\caption{Total distance of the clustering, comparison of CLARANS and CLARANS-ITP.}
\label{fig:C_CLARANS_dist}
\end{figure}

\begin{figure}[h!t]
\centering
\includegraphics[scale=0.55]{cc_time.png}
\caption{Total time of the clustering, comparison of CLARANS and CLARANS-ITP.}
\label{fig:C_CLARANS_time}
\end{figure}

\newpage
\section{Conclusion}
From the results we can conclude that CLARANS clearly outperforms PAM. When analysing the testing results of CLARANS and CLARANS-ITP we saw that with CLARANS-ITP there is 
a significant drop in the function calls to the distance calculation function. On average CLARANS-ITP used half of the function calls made by CLARANS on the same data. 
However, it seems that the implementation of CLARANS-ITP was not optimized or written well enough, as the time comparison showed that CLARANS slightly outperforms CLARANS-ITP. 

\bibliographystyle{alpha}
% References
\bibliography{references}

\section{Appendix}
The source code for the implementation of PAM, CLARANS and CLARANS-ITP is available from \url{https://bitbucket.org/Rebane/dm_project}.

\end{document}
###########
# Summary #
###########

This is a data mining course project with the goal to look at the performance of
different K-medoids algorithms and compare them. We were mostly interested in
algorithms using the triangular inequality elimination and previous medoid
index.

We have implementations of PAM, CLARANS and CLARANS-ITP.

#######################
# RUNNING THE PROJECT #
#######################

Set the project root in the PYTHONPATH variable.

For example in Unix like systems do:
    export PYTHONPATH=$PYTHONPATH:/path/to/project

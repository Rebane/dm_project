#!/usr/bin/python
import cProfile
import math
import numpy as np

from clarans2 import clarans
from pam import pam
from util import fileio, gendata

RANGE_MIN = 0
RANGE_MAX = 100

N = 3
NODES = 500
CLUSTERS = (N**2)/2 if N % 2 == 0 else (N**2)/2 + 1

# Input
#nodes = np.random.random_integers(RANGE_MIN, RANGE_MAX, size=(NODES,2))
#nodes = fileio.read_from_file('../clarans/src/nice_clusters_500_8.txt')
def distance(nds,cls):
    """Calculate the Euclidean distance. A check function."""

    d = 0
    for i in xrange(len(nds)):
        d += math.sqrt((nds[cls[i]][0] - nds[i][0])**2 + (nds[cls[i]][1] - nds[i][1])**2)
    return d

# Run tests
# PAM
def run_pam(nodes, k, i):
    clusters, medoids, tot_dist = pam.pam_basic(nodes,k)
    print 'Total distance:', tot_dist
    print 'Total distance:', distance(nodes, clusters)
    fileio.write_to_file('pam_results-%d.txt' % i, nodes, clusters)

def run_clarans(nodes, k, i):
    clusters, medoids, tot_dist = clarans.clarans_basic(nodes, k, 5, 50)
    print 'Total distance:', tot_dist
    print 'Total distance:', distance(nodes, clusters)
    fileio.write_to_file('clarans_results-%d.txt' % i, nodes, clusters)

def run_clarans_itp(nodes, k, i):
    clusters, medoids, tot_dist = clarans.clarans_itp(nodes, k, 5 ,50)
    print 'Total distance:', tot_dist
    print 'Total distance:', distance(nodes, clusters)
    fileio.write_to_file('clarans-itp_results-%d.txt' % i, nodes, clusters)

for i in range(11)[1:]:
    NODES = i*500
    for N in range(5)[2:]:
        CLUSTERS = (N**2)/2 if N % 2 == 0 else (N**2)/2 + 1

        nodes = gendata.generate_chess_board(NODES, N, NODES)

        print 'Running clustering on %d nodes, locating %d clusters...' % (len(nodes), CLUSTERS)

        print 'Running CLARANS-ITP algorithm...'
        cProfile.runctx('run_clarans_itp(nodes, k, i)', globals(), {'nodes':nodes, 'k':CLUSTERS, 'i':i})

        print 'Running CLARANS algorithm...'
        cProfile.runctx('run_clarans(nodes, k, i)', globals(), {'nodes':nodes, 'k':CLUSTERS, 'i':i})

        #print 'Running PAM algorithm...'
        #cProfile.runctx('run_pam(nodes, k, i)', globals(), {'nodes':nodes, 'k':CLUSTERS, 'i':i})

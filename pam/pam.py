import math
import numpy as np
import sys

def dist_man((x1, y1), (x2, y2)):
    """Manhattan distance function"""

    return abs(x1 - x2) + abs(y1 - y2)

def dist_euc((x1, y1), (x2, y2)):
    """Euclidean distance function"""

    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def assign_to_closest(nodes, meds, d_mat, cls=None):
    """Assign nodes to closest medoids"""

    if cls == None:
        cls = np.empty(len(nodes))

    for i in xrange(len(nodes)):
        if i in meds:
            cls[i] = i
            continue
        d = sys.maxint
        for j in xrange(len(meds)):
            d_tmp = d_mat[i,meds[j]]
            if d_tmp < d:
                d = d_tmp
                cls[i] = meds[j]

    return cls

def total_dist(d_mat, cls):
    """Calculate total distance given the medoid for each node"""

    tot_dist = 0
    for i in xrange(len(cls)):
        tot_dist += d_mat[cls[i],i]

    return tot_dist

def pam_basic(nodes, k, dist=dist_euc):
    """A basic implementation of the Partitioning Around Medoids algorithm"""

    N = len(nodes)

    print 'PAM computing...'

    # Initialize k random medoids
    meds = np.random.permutation(range(N))[:k]

    # Calculate dissimilarity matrix
    d_mat = np.asmatrix(np.zeros((N,N)))
    for i in xrange(N):
        for j in xrange(N-i-1):
            d = dist(tuple(nodes[i]),tuple(nodes[j+i+1]))
            d_mat[i,j+i+1] = d
            d_mat[j+i+1,i] = d

    # Assign nodes to closest medoids
    cls = assign_to_closest(nodes,meds,d_mat)
    
    # Calculate total distance
    tot_dist = total_dist(d_mat,cls)

    # Iterate until the system is stable
    tmp_cls = None
    change = True
    while change:
        new_meds = None
        new_cls = None
        new_tot_dist = tot_dist
        change = False
        #print 'best:', tot_dist, 'new:', new_tot_dist

        # Find the lowest distance configuration
        for i in xrange(k):
            for j in xrange(N):
                if not j in meds:
                    tmp_meds = meds.copy()
                    tmp_meds[i] = j
                    tmp_cls = assign_to_closest(nodes,tmp_meds,d_mat,tmp_cls)
                    tmp_tot_dist = total_dist(d_mat,tmp_cls)
                    if tmp_tot_dist < new_tot_dist:
                        new_meds = tmp_meds.copy()
                        new_cls = tmp_cls.copy()
                        new_tot_dist = tmp_tot_dist
                        change = True
        if change:
            meds = new_meds.copy()
            cls = new_cls.copy()
            tot_dist = new_tot_dist

    return cls, meds, tot_dist
